import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import { Layout } from 'antd';
import Home from './components/Home';
import RestaurantList from './components/RestaurantList';
import RestaurantForm from './components/RestaurantForm';
//import RestaurantDetail from './components/RestaurantDetail';
import NavigationBar from './components/common/NavigationBar';
import Footer from './components/common/Footer';

const { Content } = Layout;

const App = () => {
  useEffect(() => {
    document.title = 'Restaurant Management App';
  }, []);

  return (
    
      <Layout className="layout">
        <NavigationBar />
        <Content style={{ padding: '0 50px', flex: '1 0 auto' }}>
          <div className="site-layout-content">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/list" element={<RestaurantList />} />
              <Route path="/create" element={<RestaurantForm />} />
              {/* <Route path="/detail/:id" element={<RestaurantDetail />} /> */}
            </Routes>
          </div>
        </Content>
        <Footer />
      </Layout>
    
  );
};

export default App;
