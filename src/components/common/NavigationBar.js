// components/NavigationBar.js
import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  HomeOutlined,
  UnorderedListOutlined,
  PlusOutlined,
} from '@ant-design/icons';

const { Header } = Layout;

const NavigationBar = () => {
  return (
    <Header>
      <Menu theme="dark" mode="horizontal">
        <Menu.Item key="1" icon={<HomeOutlined />}>
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<UnorderedListOutlined />}>
          <Link to="/list">Restaurant List</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<PlusOutlined />}>
          <Link to="/create">Add Restaurant</Link>
        </Menu.Item>
      </Menu>
    </Header>
  );
};

export default NavigationBar;
