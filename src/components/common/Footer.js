// components/Footer.js
import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const AppFooter = () => {
  return (
    <Footer className='footer' style={{ textAlign: 'center' }}>
      Restaurant Management App © {new Date().getFullYear()} 
    </Footer>
  );
};

export default AppFooter;
