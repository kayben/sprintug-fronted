import React, { useState } from 'react';
import { Modal, Button } from 'antd';

const RestaurantDetail = (props) => {
    const { restaurant } = props;
    const [isModalVisible, setIsModalVisible] = useState(true);
    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
            <Modal
                title="Restaurant Details"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="cancel" onClick={handleCancel}>
                        Cancel
                    </Button>,
                ]}
            >
                <p>Name: {restaurant.name}</p>
                <p>Cuisine: {restaurant.cuisine}</p>
                <p>Location: {restaurant.location}</p>
                <img src={restaurant.image} alt={restaurant.name} style={{ maxWidth: '100%' }} />
            </Modal>
    );
};

export default RestaurantDetail;
