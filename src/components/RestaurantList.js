import React, { useState, useEffect } from 'react';
import { Card,Form, Input, Button, Modal, Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { apiRoutes } from '../api/api';

const RestaurantList = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [selectedRestaurant, setSelectedRestaurant] = useState(null);
  const [selectedEditRestaurant, setSelectedEditRestaurant] = useState(null);
  const [selectedData, setSelectedData] = useState(null);
  const [form] = Form.useForm();

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        const fetchedRestaurants = await apiRoutes.restaurants.handleFetchRestaurants();
        setSelectedData(fetchedRestaurants.data);
      } catch (error) {
        console.error('Error fetching restaurants:', error);
      }
    };

    fetchRestaurants();
  }, [setSelectedData]);

  // useEffect(() => {
  //   setSelectedEditRestaurant(null);
  // }, [selectedEditRestaurant]);



  const handleSubmit = (values) => {
    console.log(values);

    const id = values.rid;

    const updatedRestaurant = {
      name: values.name,
      cuisine: values.cuisine,
      location: values.location,
      description: values.description,
      imageUrl: values.image,
    }

    //console.log(id);

    apiRoutes.restaurants.handleUpdateRestaurant(id, updatedRestaurant);

    //if(editRestaurant.status === 'success') {
      setIsEditModalVisible(false);
    //}
    form.resetFields();
    //editRestaurant(id, updatedRestaurant);
    
  };

  const showDetail = (restaurant) => {
    
    setSelectedRestaurant(restaurant);
    setIsModalVisible(true);
  };

  const showEdit = (restaurant) => {
    
    setSelectedEditRestaurant(restaurant);
    setIsEditModalVisible(true);
    //setSelectedEditRestaurant(null);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleEditCancel = () => {
    //setSelectedEditRestaurant(null);
    setIsEditModalVisible(false);
    form.resetFields();
  };

  

  const renderRestaurantList = () => {
    if (!selectedData || selectedData.length === 0) {
      return <div>No restaurants found.</div>;
    }

    return selectedData.map((restaurant) => (
      <Card key={restaurant.id} title={restaurant.name} style={{ flex: '0 0 33.33%', margin: '16px' }}>
        <p>Cuisine: {restaurant.cuisine}</p>
        <p>Location: {restaurant.location}</p>

        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Button type="primary" ghost onClick={() => showDetail(restaurant)}>
            View
          </Button>
          <Button style={{ borderColor: 'green', color: 'green' }} type="primary" ghost onClick={() => showEdit(restaurant)}>
            Update
          </Button>
          <Button type="primary" danger ghost onClick={() => handleDelete(restaurant.rid)}>
            Delete
          </Button>
        </div>
      </Card>
    ));
  };

  const handleDelete = (restaurantId) => {
    apiRoutes.restaurants.handleDeleteRestaurant(restaurantId);
    //console.log('Deleting restaurant with ID:', restaurantId);
  };

  return (
    <div>
      <h2 style={{ textAlign: 'center' }}>Restaurant List</h2>
      <div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap' }}>
        {renderRestaurantList()}
      </div>
      {selectedRestaurant && (
        <Modal
          title="Restaurant Details"
          visible={isModalVisible}
          closable={false}
          onCancel={handleCancel}
          footer={[
            <Button key="cancel" style={{ borderColor: 'red', color: 'red' }} onClick={handleCancel}>
              Close
            </Button>,
          ]}
        >
          <p>Name: {selectedRestaurant.name}</p>
          <p>Cuisine: {selectedRestaurant.cuisine}</p>
          <p>Location: {selectedRestaurant.location}</p>
          <p>Description: {selectedRestaurant.description}</p>
          <img src={selectedRestaurant.image} alt={selectedRestaurant.name} style={{ maxWidth: '100%' }} />
        </Modal>
      )}

{selectedEditRestaurant && (
  <Modal
    title="Edit Restaurant"
    visible={isEditModalVisible}
    closable={false}
    onCancel={handleEditCancel}
    footer={[
      // <Button key="cancel" style={{ borderColor: 'red', color: 'red' }} onClick={()=>{form.resetFields();}}>
      //         Close
      //       </Button>,
    ]}
  >
    <Form form={form} initialValues={selectedEditRestaurant} onFinish={handleSubmit} >
    <Form.Item label="Id" name="rid" hidden>
      <Input />
      </Form.Item>
      <Form.Item
          name="name"
          label="Name"
          rules={[{ required: true, message: 'Please enter the restaurant name' }]}
        >
        <Input />
      </Form.Item>
      <Form.Item
          name="cuisine"
          label="Cuisine"
          rules={[{ required: true, message: 'Please enter the cuisine type' }]}
        >
        <Input />
      </Form.Item>
      <Form.Item
          name="location"
          label="Location"
          rules={[{ required: true, message: 'Please enter the restaurant location' }]}
        >
          <Input />
      </Form.Item>
      <Form.Item
          name="description"
          label="Description"
          rules={[{ required: true, message: 'Please enter the restaurant description' }]}
        >
        <Input.TextArea />
      </Form.Item>
      {/* <Form.Item label="Image" name="imageUrl" valuePropName="fileList" getValueFromEvent={(e) => e.fileList}>
              <Upload.Dragger multiple={false}>
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
              </Upload.Dragger>
            </Form.Item> */}
      <Form.Item>
          <Button style={{ borderColor: 'green', color: 'green' }}  htmlType="submit" >
            Update 
          </Button>
    
          <Button style={{ marginLeft: '8px', borderColor: 'red', color: 'red' }}  htmlType='cancel' onClick={handleEditCancel}>
            Cancel
          </Button>
        </Form.Item>
    </Form>
  </Modal>
)}

    </div>
  );
};

export default RestaurantList;
