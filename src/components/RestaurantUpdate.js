import React, { useState } from 'react';
import { Form, Input, Button } from 'antd';
import './css/RestaurantUpdate.css';

const RestaurantUpdate = ({ restaurant, onUpdate }) => {
  const [name, setName] = useState(restaurant.name);
  const [cuisine, setCuisine] = useState(restaurant.cuisine);
  const [location, setLocation] = useState(restaurant.location);
  const [image, setImage] = useState(restaurant.image);

  const handleUpdate = () => {
    const updatedRestaurant = {
      ...restaurant,
      name,
      cuisine,
      location,
      image,
    };

    onUpdate(updatedRestaurant);
  };

  return (
    <div className="restaurant-update-container">
      <h2 className="restaurant-update-title">Update Restaurant</h2>
      <Form className="restaurant-update-form">
        <Form.Item label="Name">
          <Input value={name} onChange={(e) => setName(e.target.value)} />
        </Form.Item>
        <Form.Item label="Cuisine">
          <Input value={cuisine} onChange={(e) => setCuisine(e.target.value)} />
        </Form.Item>
        <Form.Item label="Location">
          <Input value={location} onChange={(e) => setLocation(e.target.value)} />
        </Form.Item>
        <Form.Item label="Image">
          <Input value={image} onChange={(e) => setImage(e.target.value)} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" onClick={handleUpdate}>
            Update
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default RestaurantUpdate;
