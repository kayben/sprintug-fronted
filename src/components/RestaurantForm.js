import React, { useState, } from 'react';
import { Form, Input, Button } from 'antd';
import { apiRoutes } from '../api/api';
//import { Link } from 'react-router-dom';

const RestaurantForm = () => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = (values) => {

   // setIsLoading(true);
    apiRoutes.restaurants.handleCreateRestaurant(values);

    console.log('Submitting form:', values);
    // Reset the form after submission
    form.resetFields();
    setIsLoading(false);
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '80vh' }}>
      <div style={{ width: 400, margin: '0 auto' }}>
        <h2 style={{ textAlign: 'center' }}>Add New Restaurant</h2>
        <Form form={form} onFinish={handleSubmit}>
          <Form.Item
            name="name"
            label="Name"
            rules={[{ required: true, message: 'Please enter the restaurant name' }]}
          >
            <Input placeholder="Restaurant Name" />
          </Form.Item>
          <Form.Item
            name="cuisine"
            label="Cuisine"
            rules={[{ required: true, message: 'Please enter the cuisine type' }]}
          >
            <Input placeholder="Cuisine Type" />
          </Form.Item>
          <Form.Item
            name="location"
            label="Location"
            rules={[{ required: true, message: 'Please enter the restaurant location' }]}
          >
            <Input placeholder="Location" />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            rules={[{ required: true, message: 'Please enter the restaurant description' }]}
          >
            <Input.TextArea placeholder="Description" />
          </Form.Item>

          <Form.Item
            name="imageUrl"
            label="Image"
            rules={[{ required: true, message: 'Please upload an image' }]}
          >
            <Input type="file" />
          </Form.Item>
          <Form.Item>
            <Button style={{ borderColor: 'green', color: 'green' }} htmlType="submit" loading={isLoading}>
              Add
            </Button>
            <Button style={{ marginLeft: '8px', borderColor: 'red', color: 'red' }} onClick={() => form.resetFields()}>
              Cancel
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default RestaurantForm;
