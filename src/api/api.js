import axios from 'axios';

const API_BASE_URL = 'http://localhost:4500'; 

// Restaurant API routes
const restaurantRoutes = {
    handleFetchRestaurants: async () => {
        try {
            const response = await axios
                .get(`${API_BASE_URL}/restaurants`);
            console.log('Restaurant fetched:', response.data);
            return response.data;
        } catch (error) {
            console.log(error);
            console.error('Error fetching restaurant:', error);
        }
    },

    handleCreateRestaurant: async (newRestaurant) => {
        try {
            const response = await axios.post(`${API_BASE_URL}/create-restaurant`,
                {
                    name: newRestaurant.name, cuisine: newRestaurant.cuisine, location: newRestaurant.location,
                    description: newRestaurant.description, imageUrl: newRestaurant.imageUrl
                })
            console.log('Restaurant created:', response.data);
            return response.data;
        } catch (error) {
            console.log(error);
            console.error('Error creating restaurant:', error);
        }

    },

    handleUpdateRestaurant: async (id,newRestaurant) => {
        try {
            const response = await axios.put(`${API_BASE_URL}/update-restaurant/${id}`, {
                name: newRestaurant.name, cuisine: newRestaurant.cuisine, location: newRestaurant.location,
                description: newRestaurant.description, imageUrl: newRestaurant.imageUrl
            })
            console.log('Restaurant updated:', response.data);
            return response.data;
        } catch (error) {
            console.log(error);
            console.error('Error updating restaurant:', error);
        }

    },

    handleDeleteRestaurant: async (id) => {
        try {
            const response = await axios.delete(`${API_BASE_URL}/delete-restaurant/${id}`);
            console.log('Restaurant updated:', response.data);
            return response.data;
        } catch (error) {
            console.log(error);
            console.error('Error deleting restaurant:', error);
        }
        
    },
};


export const apiRoutes = {
    restaurants: restaurantRoutes,
};
