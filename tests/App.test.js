import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

describe('Restaurant Management App', () => {
  test('renders restaurant list', () => {
    render(<App />);
    const restaurantListElement = screen.getByText(/Restaurant List/i);
    expect(restaurantListElement).toBeInTheDocument();
  });

  test('renders restaurant details modal', () => {
    render(<App />);
    const viewButton = screen.getByText(/View/i);
    fireEvent.click(viewButton);
    const modalTitle = screen.getByText(/Restaurant Details/i);
    expect(modalTitle).toBeInTheDocument();
  });

  test('creates a new restaurant', () => {
    render(<App />);
    const addButton = screen.getByText(/Add Restaurant/i);
    fireEvent.click(addButton);
    const nameInput = screen.getByLabelText(/Name/i);
    const cuisineInput = screen.getByLabelText(/Cuisine/i);
    const locationInput = screen.getByLabelText(/Location/i);
    const submitButton = screen.getByText(/Submit/i);

    fireEvent.change(nameInput, { target: { value: 'New Restaurant' } });
    fireEvent.change(cuisineInput, { target: { value: 'Italian' } });
    fireEvent.change(locationInput, { target: { value: 'New York' } });
    fireEvent.click(submitButton);

    const newRestaurant = screen.getByText(/New Restaurant/i);
    expect(newRestaurant).toBeInTheDocument();
  });

  test('deletes a restaurant', () => {
    render(<App />);
    const deleteButton = screen.getAllByText(/Delete/i)[0];
    fireEvent.click(deleteButton);

    const deletedRestaurant = screen.queryByText(/Restaurant A/i);
    expect(deletedRestaurant).not.toBeInTheDocument();
  });

});
