# Restaurant Management App (Frontend)
## Features

- **Restaurant Listing**: Display a list of restaurants.
- **Restaurant Creation**: Add new restaurants.
- **Restaurant Details**: View detailed information about a selected restaurant.
- **Restaurant Update**: Edit the details of a restaurant.
- **Restaurant Deletion**: Delete a restaurant.

## Technologies Used

- React
- React Router
- Ant Design

## Setup and Installation

1. Clone the repository or download the source code.
2. Navigate to the project root directory.
3. Install the required dependencies by running `yarn install`.
4. Start the development server by running `yarn start`.
5. Access the application in your web browser at `http://localhost:3000`.

## File Structure

- `public`: Contains the public assets and the main HTML file.
- `src`: Contains the source code for the React components and styles.
  - `components`: Contains the reusable components used in the application.
  - `api`: Contains the axios requests for the application.
  - `css`: Contains the css files for the components.
  - `App.js`: The main component that serves as the entry point for the application.
  - `index.js`: The file that renders the root component and mounts it into the DOM.



